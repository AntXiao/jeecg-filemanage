/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50726
Source Host           : 127.0.0.1:3306
Source Database       : trainal

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2021-04-13 14:11:18
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for dtg_demand_details
-- ----------------------------
DROP TABLE IF EXISTS `dtg_demand_details`;
CREATE TABLE `dtg_demand_details` (
  `id` varchar(40) NOT NULL COMMENT '编号',
  `pid` varchar(40) DEFAULT NULL COMMENT '父编号',
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) DEFAULT NULL COMMENT '更新人',
  `sys_org_code` varchar(50) DEFAULT NULL COMMENT '所属部门',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  `eqp_no` varchar(50) DEFAULT NULL COMMENT '设备编号',
  `eqp_num` decimal(10,0) DEFAULT NULL COMMENT '需求数量',
  `eqp_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资料需求明细';

-- ----------------------------
-- Table structure for dtg_demand_info
-- ----------------------------
DROP TABLE IF EXISTS `dtg_demand_info`;
CREATE TABLE `dtg_demand_info` (
  `id` varchar(40) NOT NULL COMMENT '编号',
  `demand_no` varchar(50) DEFAULT NULL COMMENT '需求编号',
  `branch_id` varchar(36) DEFAULT NULL COMMENT '支队id',
  `name` varchar(255) DEFAULT NULL COMMENT '需求单名称',
  `memo` varchar(4000) DEFAULT NULL COMMENT '备注',
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) DEFAULT NULL COMMENT '更新人',
  `sys_org_code` varchar(50) DEFAULT NULL COMMENT '所属部门',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资料需求单';

-- ----------------------------
-- Table structure for dtg_file_details_history
-- ----------------------------
DROP TABLE IF EXISTS `dtg_file_details_history`;
CREATE TABLE `dtg_file_details_history` (
  `id` varchar(36) NOT NULL COMMENT 'id',
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) DEFAULT NULL COMMENT '所属部门',
  `secret_lev` int(10) DEFAULT NULL COMMENT '密级',
  `file_code` varchar(36) DEFAULT NULL COMMENT '编号',
  `file_title` varchar(128) NOT NULL COMMENT '文件名',
  `org_belong` varchar(128) DEFAULT NULL COMMENT '编制单位',
  `file_description` varchar(1024) DEFAULT NULL COMMENT '文件描述',
  `boat_belong` varchar(36) DEFAULT NULL COMMENT '所属舰船冗余',
  `sys_belong` varchar(64) DEFAULT NULL COMMENT '所属系统',
  `eqp_belong` varchar(64) DEFAULT NULL COMMENT '所属装备',
  `fileid` varchar(36) DEFAULT NULL COMMENT '文件id',
  `opt` int(3) DEFAULT NULL COMMENT '0无操作，1新增版本，2修改，3删除',
  `version_no` double(10,1) DEFAULT NULL COMMENT '当时操作的版本号',
  `file_path` varchar(256) DEFAULT NULL COMMENT '文件路径',
  `file_size` int(10) DEFAULT NULL COMMENT '文件大小(B)',
  `file_types` varchar(36) DEFAULT NULL COMMENT '文件类型',
  `file_mix_name` varchar(128) DEFAULT NULL COMMENT '混淆名',
  `pid` varchar(36) DEFAULT NULL COMMENT '上级节点id',
  `model_name` varchar(36) DEFAULT NULL COMMENT '模块名',
  `side_num` varchar(36) DEFAULT NULL COMMENT '舷号',
  `eqp_uid` varchar(36) DEFAULT NULL COMMENT '所属设备uuid',
  `system_no` varchar(50) DEFAULT NULL COMMENT '所属系统编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for dtg_file_details_info
-- ----------------------------
DROP TABLE IF EXISTS `dtg_file_details_info`;
CREATE TABLE `dtg_file_details_info` (
  `id` varchar(36) NOT NULL COMMENT '主键',
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) DEFAULT NULL COMMENT '所属部门',
  `secret_lev` int(11) DEFAULT '0' COMMENT '密级',
  `file_code` varchar(32) DEFAULT NULL COMMENT '编号',
  `file_title` varchar(128) NOT NULL COMMENT '文件名',
  `org_belong` varchar(128) DEFAULT NULL COMMENT '编制单位',
  `file_description` varchar(1024) DEFAULT NULL COMMENT '文件描述',
  `boat_belong` varchar(36) DEFAULT NULL COMMENT '所属舰船冗余',
  `sys_belong` varchar(64) DEFAULT NULL COMMENT '所属系统',
  `eqp_belong` varchar(64) DEFAULT NULL COMMENT '所属装备',
  `fileid` varchar(36) DEFAULT NULL COMMENT '文件id',
  `file_path` varchar(256) NOT NULL COMMENT '文件路径',
  `file_size` int(10) DEFAULT NULL COMMENT '文档大小(B)',
  `file_types` varchar(36) DEFAULT NULL COMMENT '文档格式',
  `file_mix_name` varchar(128) DEFAULT NULL COMMENT '混淆名',
  `pid` varchar(36) DEFAULT NULL COMMENT '上级节点id',
  `model_name` varchar(36) DEFAULT NULL COMMENT '模块名',
  `side_num` varchar(36) DEFAULT NULL COMMENT '船舷号',
  `eqp_uid` varchar(36) DEFAULT NULL COMMENT '所属装备uuid',
  `system_no` varchar(50) DEFAULT NULL COMMENT '所属系统编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for dtg_file_dir_info
-- ----------------------------
DROP TABLE IF EXISTS `dtg_file_dir_info`;
CREATE TABLE `dtg_file_dir_info` (
  `id` varchar(36) NOT NULL,
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) DEFAULT NULL COMMENT '所属部门',
  `version_no` double DEFAULT '1' COMMENT '版本号',
  `fileid` varchar(32) DEFAULT NULL COMMENT '文件id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for dtg_file_drc_details
-- ----------------------------
DROP TABLE IF EXISTS `dtg_file_drc_details`;
CREATE TABLE `dtg_file_drc_details` (
  `id` varchar(36) NOT NULL,
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) DEFAULT NULL COMMENT '所属部门',
  `drc_type` int(11) DEFAULT '0' COMMENT '操作类型0阅读1下载2收藏',
  `fileid` varchar(32) DEFAULT NULL COMMENT '文件id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for dtg_file_drc_info
-- ----------------------------
DROP TABLE IF EXISTS `dtg_file_drc_info`;
CREATE TABLE `dtg_file_drc_info` (
  `id` varchar(36) NOT NULL,
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) DEFAULT NULL COMMENT '所属部门',
  `download_times` int(11) DEFAULT '0' COMMENT '下载次数',
  `read_times` int(11) DEFAULT '0' COMMENT '阅读次数',
  `collect_times` int(11) DEFAULT '0' COMMENT '收藏次数',
  `fileid` varchar(32) DEFAULT NULL COMMENT '关联文件表id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for dtg_file_export_rec
-- ----------------------------
DROP TABLE IF EXISTS `dtg_file_export_rec`;
CREATE TABLE `dtg_file_export_rec` (
  `id` varchar(36) NOT NULL COMMENT '主键',
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) DEFAULT NULL COMMENT '所属部门',
  `fileid` varchar(32) DEFAULT NULL COMMENT '文件id',
  `pid` varchar(32) DEFAULT NULL COMMENT '上级id',
  `file_title` varchar(128) DEFAULT NULL COMMENT '文件名称',
  `typename` varchar(32) DEFAULT NULL COMMENT '类型名称',
  `belong` varchar(32) DEFAULT NULL COMMENT '所属上级名称',
  `branch_id` varchar(32) DEFAULT NULL COMMENT '导出支队id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for dtg_type_details
-- ----------------------------
DROP TABLE IF EXISTS `dtg_type_details`;
CREATE TABLE `dtg_type_details` (
  `id` varchar(36) NOT NULL COMMENT '主键',
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) DEFAULT NULL COMMENT '所属部门',
  `type_name` varchar(128) DEFAULT NULL COMMENT '分类名称',
  `remark` varchar(512) DEFAULT NULL COMMENT '备注',
  `pid` varchar(32) DEFAULT NULL COMMENT '上级节点ID',
  `lv_idx` int(11) DEFAULT NULL COMMENT '级别序号',
  `lv_index` int(11) DEFAULT NULL COMMENT '级别内排序号',
  `has_child` varchar(3) DEFAULT NULL COMMENT '是否有子节点',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
