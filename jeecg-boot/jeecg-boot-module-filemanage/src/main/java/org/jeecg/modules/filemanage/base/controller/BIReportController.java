package org.jeecg.modules.filemanage.base.controller;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.modules.filemanage.base.service.IBIReportService;
import org.jeecg.modules.filemanage.base.vo.fileInOutView;
import org.jeecg.modules.filemanage.base.vo.fileSearchView;
import org.jeecg.modules.filemanage.base.vo.fileTypeRadioView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Description: 表格数据表
 * @Author: jeecg-boot
 * @Date:   2020-12-31
 * @Version: V1.0
 */
@Api(tags="BI报表")
@RestController
@RequestMapping("/biReport")
@Slf4j
public class BIReportController {
    @Autowired
    private IBIReportService ibiReportService;

    @AutoLog(value = "文件类型正态分布")
    @ApiOperation(value="文件类型正态分布", notes="文件类型正态分布")
    @GetMapping(value = "/fileTypeRadio")
    public Result<?> fileTypeRadio(String fileType) {

       List<fileTypeRadioView> res = ibiReportService.fileTypeRadio(fileType);
       return res.size() >-1 ?  Result.ok(res): Result.error("查询结果为空");
    }

    @AutoLog(value = "文件资料供需图")
    @ApiOperation(value="文件资料供需图", notes="文件资料供需图")
    @GetMapping(value = "/fileInOut")
    public Result<?> fileInOut(String branchId) {

        List<fileInOutView> res = ibiReportService.fileInOut(branchId);
        return res.size() >-1 ?  Result.ok(res): Result.error("查询结果为空");
    }

    @AutoLog(value = "文件随机搜索")
    @ApiOperation(value="文件随机搜索", notes="文件随机搜索")
    @GetMapping(value = "/fileSearch")
    public Result<?> fileSearch(
            @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
            @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
            String keyword,
            @RequestParam(name="typeName", required = false) String  typeName,
            @RequestParam(name="suffix", required = false) String suffix
    ) {
        Result<Page<fileSearchView>> result = new Result<Page<fileSearchView>>();
        Page<fileSearchView> pageList = new Page<fileSearchView>(pageNo,pageSize);
//        List<fileSearchView> res = ibiReportService.fileSearch(keyword);
        pageList = ibiReportService.fileSearch(pageList,keyword,typeName,suffix);//热搜的文件
        result.setSuccess(true);
        result.setResult(pageList);
        return result;
    }

    @AutoLog(value = "文件热搜榜")
    @ApiOperation(value="文件热搜榜", notes="文件热搜榜")
    @GetMapping(value = "/fileHot")
    public Result<?> fileHot(
            @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
            @RequestParam(name="pageSize", defaultValue="10") Integer pageSize
    ) {
        Result<Page<fileSearchView>> result = new Result<Page<fileSearchView>>();
        Page<fileSearchView> pageList = new Page<fileSearchView>(pageNo,pageSize);
        pageList = ibiReportService.fileHot(pageList,"","1");//热搜的文件
        result.setSuccess(true);
        result.setResult(pageList);
        return result;
    }

    @AutoLog(value = "获取文档总数和注册用户数")
    @ApiOperation(value="获取文档总数和注册用户数", notes="获取文档总数和注册用户数")
    @GetMapping(value = "/totalDoc")
    public Result<?> totalDoc(

    ) {
        fileInOutView res = ibiReportService.totalDoc();
        return res!=null ?  Result.ok(res): Result.error("查询结果为空");
    }

    @AutoLog(value = "文件热搜榜")
    @ApiOperation(value="文件热搜榜", notes="文件热搜榜")
    @GetMapping(value = "/fileNew")
    public Result<?> fileNew(
            @RequestParam(name="typeId", required = false) String typeId,
            @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
            @RequestParam(name="pageSize", defaultValue="10") Integer pageSize
    ) {
        Result<Page<fileSearchView>> result = new Result<Page<fileSearchView>>();
        Page<fileSearchView> pageList = new Page<fileSearchView>(pageNo,pageSize);
        pageList = ibiReportService.fileNew(pageList,typeId,"1");//热搜的文件
        result.setSuccess(true);
        result.setResult(pageList);
        return result;
    }

    @AutoLog(value = "获取当前人文件数据")
    @ApiOperation(value="获取当前人文件数据", notes="获取当前人文件数据")
    @GetMapping(value = "/userFileInfo")
    public Result<?> userFileInfo(
            @RequestParam(name="userid", required = true) String userid
    ) {
        fileInOutView res = ibiReportService.userFileInfo(userid);
        return  Result.ok(res);
    }

    @AutoLog(value = "关键字查询es")
    @ApiOperation(value="关键字查询es", notes="关键字查询es")
    @GetMapping(value = "/essearch")
    public Result<?> essearch(
            @RequestParam(name="kw", required = true) String kw
    ) {
        List<String> res = ibiReportService.essearch(kw);
        return  Result.ok(res);
    }

}
