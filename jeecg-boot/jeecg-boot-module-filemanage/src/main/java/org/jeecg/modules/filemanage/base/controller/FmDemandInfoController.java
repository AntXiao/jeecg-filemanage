package org.jeecg.modules.filemanage.base.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.filemanage.base.entity.FmDemandInfo;
import org.jeecg.modules.filemanage.base.service.IFmDemandInfoService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: fm_demand_info
 * @Author: jeecg-boot
 * @Date:   2021-04-13
 * @Version: V1.0
 */
@Api(tags="fm_demand_info")
@RestController
@RequestMapping("/base/fmDemandInfo")
@Slf4j
public class FmDemandInfoController extends JeecgController<FmDemandInfo, IFmDemandInfoService> {
	@Autowired
	private IFmDemandInfoService fmDemandInfoService;
	
	/**
	 * 分页列表查询
	 *
	 * @param fmDemandInfo
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "fm_demand_info-分页列表查询")
	@ApiOperation(value="fm_demand_info-分页列表查询", notes="fm_demand_info-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(FmDemandInfo fmDemandInfo,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<FmDemandInfo> queryWrapper = QueryGenerator.initQueryWrapper(fmDemandInfo, req.getParameterMap());
		Page<FmDemandInfo> page = new Page<FmDemandInfo>(pageNo, pageSize);
		IPage<FmDemandInfo> pageList = fmDemandInfoService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param fmDemandInfo
	 * @return
	 */
	@AutoLog(value = "fm_demand_info-添加")
	@ApiOperation(value="fm_demand_info-添加", notes="fm_demand_info-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody FmDemandInfo fmDemandInfo) {
		fmDemandInfoService.save(fmDemandInfo);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param fmDemandInfo
	 * @return
	 */
	@AutoLog(value = "fm_demand_info-编辑")
	@ApiOperation(value="fm_demand_info-编辑", notes="fm_demand_info-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody FmDemandInfo fmDemandInfo) {
		fmDemandInfoService.updateById(fmDemandInfo);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "fm_demand_info-通过id删除")
	@ApiOperation(value="fm_demand_info-通过id删除", notes="fm_demand_info-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		fmDemandInfoService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "fm_demand_info-批量删除")
	@ApiOperation(value="fm_demand_info-批量删除", notes="fm_demand_info-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.fmDemandInfoService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "fm_demand_info-通过id查询")
	@ApiOperation(value="fm_demand_info-通过id查询", notes="fm_demand_info-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		FmDemandInfo fmDemandInfo = fmDemandInfoService.getById(id);
		if(fmDemandInfo==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(fmDemandInfo);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param fmDemandInfo
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, FmDemandInfo fmDemandInfo) {
        return super.exportXls(request, fmDemandInfo, FmDemandInfo.class, "fm_demand_info");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, FmDemandInfo.class);
    }

}
