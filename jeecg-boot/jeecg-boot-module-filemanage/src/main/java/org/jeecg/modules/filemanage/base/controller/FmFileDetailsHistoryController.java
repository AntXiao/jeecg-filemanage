package org.jeecg.modules.filemanage.base.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.filemanage.base.entity.FmFileDetailsHistory;
import org.jeecg.modules.filemanage.base.service.IFmFileDetailsHistoryService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: fm_file_details_history
 * @Author: jeecg-boot
 * @Date:   2021-04-13
 * @Version: V1.0
 */
@Api(tags="fm_file_details_history")
@RestController
@RequestMapping("/base/fmFileDetailsHistory")
@Slf4j
public class FmFileDetailsHistoryController extends JeecgController<FmFileDetailsHistory, IFmFileDetailsHistoryService> {
	@Autowired
	private IFmFileDetailsHistoryService fmFileDetailsHistoryService;
	
	/**
	 * 分页列表查询
	 *
	 * @param fmFileDetailsHistory
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "fm_file_details_history-分页列表查询")
	@ApiOperation(value="fm_file_details_history-分页列表查询", notes="fm_file_details_history-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(FmFileDetailsHistory fmFileDetailsHistory,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<FmFileDetailsHistory> queryWrapper = QueryGenerator.initQueryWrapper(fmFileDetailsHistory, req.getParameterMap());
		Page<FmFileDetailsHistory> page = new Page<FmFileDetailsHistory>(pageNo, pageSize);
		IPage<FmFileDetailsHistory> pageList = fmFileDetailsHistoryService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param fmFileDetailsHistory
	 * @return
	 */
	@AutoLog(value = "fm_file_details_history-添加")
	@ApiOperation(value="fm_file_details_history-添加", notes="fm_file_details_history-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody FmFileDetailsHistory fmFileDetailsHistory) {
		fmFileDetailsHistoryService.save(fmFileDetailsHistory);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param fmFileDetailsHistory
	 * @return
	 */
	@AutoLog(value = "fm_file_details_history-编辑")
	@ApiOperation(value="fm_file_details_history-编辑", notes="fm_file_details_history-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody FmFileDetailsHistory fmFileDetailsHistory) {
		fmFileDetailsHistoryService.updateById(fmFileDetailsHistory);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "fm_file_details_history-通过id删除")
	@ApiOperation(value="fm_file_details_history-通过id删除", notes="fm_file_details_history-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		fmFileDetailsHistoryService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "fm_file_details_history-批量删除")
	@ApiOperation(value="fm_file_details_history-批量删除", notes="fm_file_details_history-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.fmFileDetailsHistoryService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "fm_file_details_history-通过id查询")
	@ApiOperation(value="fm_file_details_history-通过id查询", notes="fm_file_details_history-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		FmFileDetailsHistory fmFileDetailsHistory = fmFileDetailsHistoryService.getById(id);
		if(fmFileDetailsHistory==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(fmFileDetailsHistory);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param fmFileDetailsHistory
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, FmFileDetailsHistory fmFileDetailsHistory) {
        return super.exportXls(request, fmFileDetailsHistory, FmFileDetailsHistory.class, "fm_file_details_history");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, FmFileDetailsHistory.class);
    }

}
