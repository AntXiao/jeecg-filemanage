package org.jeecg.modules.filemanage.base.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.filemanage.base.entity.FmFileDetailsInfo;
import org.jeecg.modules.filemanage.base.service.IFmFileDetailsInfoService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.filemanage.base.vo.DMFileList;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: fm_file_details_info
 * @Author: jeecg-boot
 * @Date:   2021-04-13
 * @Version: V1.0
 */
@Api(tags="fm_file_details_info")
@RestController
@RequestMapping("/base/fmFileDetailsInfo")
@Slf4j
public class FmFileDetailsInfoController extends JeecgController<FmFileDetailsInfo, IFmFileDetailsInfoService> {
	@Autowired
	private IFmFileDetailsInfoService fmFileDetailsInfoService;

	/**
	 * 分页列表查询
	 *
	 * @param fmFileDetailsInfo
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "fm_file_details_info-分页列表查询")
	@ApiOperation(value="fm_file_details_info-分页列表查询", notes="fm_file_details_info-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(FmFileDetailsInfo fmFileDetailsInfo,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<FmFileDetailsInfo> queryWrapper = QueryGenerator.initQueryWrapper(fmFileDetailsInfo, req.getParameterMap());
		Page<FmFileDetailsInfo> page = new Page<FmFileDetailsInfo>(pageNo, pageSize);
		IPage<FmFileDetailsInfo> pageList = fmFileDetailsInfoService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	 /**
	  * 分页列表查询
	  *
	  * @param dtgFileDetailsInfo
	  * @param pageNo
	  * @param pageSize
	  * @param req
	  * @return
	  */
	 @AutoLog(value = "dtg_file_details_info_nopid-分页未挂接列表查询")
	 @ApiOperation(value="dtg_file_details_info_nopid-分页未挂接列表查询", notes="dtg_file_details_info_nopid-分页未挂接列表查询")
	 @GetMapping(value = "/listNopid")
	 public Result<?> queryPagelistNopid(FmFileDetailsInfo dtgFileDetailsInfo,
										 @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
										 @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
										 HttpServletRequest req) {
		 Result<Page<FmFileDetailsInfo>> result = new Result<Page<FmFileDetailsInfo>>();
		 Page<FmFileDetailsInfo> pageList = new Page<FmFileDetailsInfo>(pageNo,pageSize);
		 pageList = fmFileDetailsInfoService.queryPagelistNopid(pageList,"","1");//无父级的文件
		 result.setSuccess(true);
		 result.setResult(pageList);
		 return result;
	 }

	/**
	 *   添加
	 *
	 * @param fmFileDetailsInfo
	 * @return
	 */
	@AutoLog(value = "fm_file_details_info-添加")
	@ApiOperation(value="fm_file_details_info-添加", notes="fm_file_details_info-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody FmFileDetailsInfo fmFileDetailsInfo) {
		fmFileDetailsInfoService.save(fmFileDetailsInfo);
		return Result.ok("添加成功！");
	}

	/**
	 *  编辑
	 *
	 * @param fmFileDetailsInfo
	 * @return
	 */
	@AutoLog(value = "fm_file_details_info-编辑")
	@ApiOperation(value="fm_file_details_info-编辑", notes="fm_file_details_info-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody FmFileDetailsInfo fmFileDetailsInfo) {
		fmFileDetailsInfoService.updateById(fmFileDetailsInfo);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "fm_file_details_info-通过id删除")
	@ApiOperation(value="fm_file_details_info-通过id删除", notes="fm_file_details_info-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		fmFileDetailsInfoService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "fm_file_details_info-批量删除")
	@ApiOperation(value="fm_file_details_info-批量删除", notes="fm_file_details_info-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.fmFileDetailsInfoService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "fm_file_details_info-通过id查询")
	@ApiOperation(value="fm_file_details_info-通过id查询", notes="fm_file_details_info-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		FmFileDetailsInfo fmFileDetailsInfo = fmFileDetailsInfoService.getById(id);
		if(fmFileDetailsInfo==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(fmFileDetailsInfo);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param fmFileDetailsInfo
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, FmFileDetailsInfo fmFileDetailsInfo) {
        return super.exportXls(request, fmFileDetailsInfo, FmFileDetailsInfo.class, "fm_file_details_info");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, FmFileDetailsInfo.class);
    }

	 /**
	  * 通过pid查询
	  *
	  * @param pid
	  * @return
	  */
	 @AutoLog(value = "技术资料文件详情表-通过pid查询")
	 @ApiOperation(value="技术资料文件详情表-通过pid查询", notes="技术资料文件详情表-通过pid查询")
	 @GetMapping(value = "/getByPid")
	 public Result<?> getByPid(@RequestParam(name="pid",required=true) String pid,
							   @RequestParam(name="fileTitle",required=false) String fileTitle,
							   @RequestParam(name="fileCode",required=false) String fileCode,
							   @RequestParam(name="secretLev",required=false) Integer secretLev,
							   @RequestParam(name="eqpBelong",required=false) String eqpBelong,
							   @RequestParam(name="type",required=false) Integer type
	 ) {

		 List<DMFileList> fileDetailsInfo = fmFileDetailsInfoService.getByPid(pid, fileTitle, fileCode, secretLev, eqpBelong,type);

		 return Result.ok(fileDetailsInfo);
	 }

	 /**
	  * 通过pid查询
	  *
	  * @param
	  * @return
	  */
	 @AutoLog(value = "文件详情表-设置pid")
	 @ApiOperation(value="文件详情表-设置pid", notes="文件详情表-设置pid")
	 @GetMapping(value = "/setPid")
	 public Result<?> setPid (@RequestParam(name="ids",required=true) String ids,
							  @RequestParam(name="pid",required=true) String pid
							  )
	 {
	 	boolean res = fmFileDetailsInfoService.setPid(ids,pid);
	 	return res ==true ?Result.ok("挂接成功"):Result.error("挂接失败");
	 }
}
