package org.jeecg.modules.filemanage.base.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.filemanage.base.entity.FmFileDrcDetails;
import org.jeecg.modules.filemanage.base.service.IFmFileDrcDetailsService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.filemanage.base.vo.DMFileList;
import org.jeecg.modules.filemanage.base.vo.FileDrcMyList;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: fm_file_drc_details
 * @Author: jeecg-boot
 * @Date:   2021-04-13
 * @Version: V1.0
 */
@Api(tags="fm_file_drc_details")
@RestController
@RequestMapping("/base/fmFileDrcDetails")
@Slf4j
public class FmFileDrcDetailsController extends JeecgController<FmFileDrcDetails, IFmFileDrcDetailsService> {
	@Autowired
	private IFmFileDrcDetailsService fmFileDrcDetailsService;

	/**
	 * 分页列表查询
	 *
	 * @param fmFileDrcDetails
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "fm_file_drc_details-分页列表查询")
	@ApiOperation(value="fm_file_drc_details-分页列表查询", notes="fm_file_drc_details-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(FmFileDrcDetails fmFileDrcDetails,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<FmFileDrcDetails> queryWrapper = QueryGenerator.initQueryWrapper(fmFileDrcDetails, req.getParameterMap());
		Page<FmFileDrcDetails> page = new Page<FmFileDrcDetails>(pageNo, pageSize);
		IPage<FmFileDrcDetails> pageList = fmFileDrcDetailsService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
	 *   添加
	 *
	 * @param fmFileDrcDetails
	 * @return
	 */
	@AutoLog(value = "fm_file_drc_details-添加")
	@ApiOperation(value="fm_file_drc_details-添加", notes="fm_file_drc_details-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody FmFileDrcDetails fmFileDrcDetails) {
		fmFileDrcDetailsService.save(fmFileDrcDetails);
		return Result.ok("添加成功！");
	}

	/**
	 *  编辑
	 *
	 * @param fmFileDrcDetails
	 * @return
	 */
	@AutoLog(value = "fm_file_drc_details-编辑")
	@ApiOperation(value="fm_file_drc_details-编辑", notes="fm_file_drc_details-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody FmFileDrcDetails fmFileDrcDetails) {
		fmFileDrcDetailsService.updateById(fmFileDrcDetails);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "fm_file_drc_details-通过id删除")
	@ApiOperation(value="fm_file_drc_details-通过id删除", notes="fm_file_drc_details-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		fmFileDrcDetailsService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "fm_file_drc_details-批量删除")
	@ApiOperation(value="fm_file_drc_details-批量删除", notes="fm_file_drc_details-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.fmFileDrcDetailsService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "fm_file_drc_details-通过id查询")
	@ApiOperation(value="fm_file_drc_details-通过id查询", notes="fm_file_drc_details-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		FmFileDrcDetails fmFileDrcDetails = fmFileDrcDetailsService.getById(id);
		if(fmFileDrcDetails==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(fmFileDrcDetails);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param fmFileDrcDetails
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, FmFileDrcDetails fmFileDrcDetails) {
        return super.exportXls(request, fmFileDrcDetails, FmFileDrcDetails.class, "fm_file_drc_details");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, FmFileDrcDetails.class);
    }
	 /**
	  * 通过pid查询
	  *
	  * @param pid
	  * @return
	  */
	 @AutoLog(value = "收藏文件详情表-通过pid查询")
	 @ApiOperation(value="收藏文件详情表-通过pid查询", notes="收藏文件详情表-通过pid查询")
	 @GetMapping(value = "/getByPid")
	 public Result<?> getByPid(@RequestParam(name="pid",required=true) String pid,
							   @RequestParam(name="userid",required=true) String userid,
							   @RequestParam(name="fileTitle",required=false) String fileTitle,
							   @RequestParam(name="fileCode",required=false) String fileCode,
							   @RequestParam(name="secretLev",required=false) Integer secretLev,
							   @RequestParam(name="fileTypes",required=false) String fileTypes
	 ) {
		 List<DMFileList> fileDetailsInfo = fmFileDrcDetailsService.getByPid(pid,userid,fileTitle,fileCode,secretLev,fileTypes);

		 return Result.ok(fileDetailsInfo);
	 }

	 /**
	  * 个人中心列表查询
	  *
	  * @param fmFileDrcInfo
	  * @param pageNo
	  * @param pageSize
	  * @param req
	  * @return
	  */
	 @AutoLog(value = "fm_file_drc_details-个人中心列表查询")
	 @ApiOperation(value="fm_file_drc_details-个人中心列表查询", notes="fm_file_drc_details-个人中心列表查询")
	 @GetMapping(value = "/mylist")
	 public Result<?> mylist(String createBy,
							 Integer drcType,
							 @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
							 @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
							 HttpServletRequest req) {
		 Result<Page<FileDrcMyList>> result = new Result<Page<FileDrcMyList>>();
		 Page<FileDrcMyList> pageList = new Page<FileDrcMyList>(pageNo,pageSize);
		 pageList = fmFileDrcDetailsService.mylist(pageList,createBy,drcType);//无父级的文件
		 result.setSuccess(true);
		 result.setResult(pageList);
		 return result;
	 }
}
