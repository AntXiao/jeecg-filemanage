package org.jeecg.modules.filemanage.base.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.filemanage.base.entity.FmFileDrcInfo;
import org.jeecg.modules.filemanage.base.service.IFmFileDrcInfoService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.filemanage.base.vo.DRCByFileid;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: fm_file_drc_info
 * @Author: jeecg-boot
 * @Date:   2021-04-13
 * @Version: V1.0
 */
@Api(tags="fm_file_drc_info")
@RestController
@RequestMapping("/base/fmFileDrcInfo")
@Slf4j
public class FmFileDrcInfoController extends JeecgController<FmFileDrcInfo, IFmFileDrcInfoService> {
	@Autowired
	private IFmFileDrcInfoService fmFileDrcInfoService;

	/**
	 * 分页列表查询
	 *
	 * @param fmFileDrcInfo
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "fm_file_drc_info-分页列表查询")
	@ApiOperation(value="fm_file_drc_info-分页列表查询", notes="fm_file_drc_info-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(FmFileDrcInfo fmFileDrcInfo,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<FmFileDrcInfo> queryWrapper = QueryGenerator.initQueryWrapper(fmFileDrcInfo, req.getParameterMap());
		Page<FmFileDrcInfo> page = new Page<FmFileDrcInfo>(pageNo, pageSize);
		IPage<FmFileDrcInfo> pageList = fmFileDrcInfoService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
	 *   添加
	 *
	 * @param fmFileDrcInfo
	 * @return
	 */
	@AutoLog(value = "fm_file_drc_info-添加")
	@ApiOperation(value="fm_file_drc_info-添加", notes="fm_file_drc_info-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody FmFileDrcInfo fmFileDrcInfo) {
		fmFileDrcInfoService.save(fmFileDrcInfo);
		return Result.ok("添加成功！");
	}

	/**
	 *  编辑
	 *
	 * @param fmFileDrcInfo
	 * @return
	 */
	@AutoLog(value = "fm_file_drc_info-编辑")
	@ApiOperation(value="fm_file_drc_info-编辑", notes="fm_file_drc_info-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody FmFileDrcInfo fmFileDrcInfo) {
		fmFileDrcInfoService.updateById(fmFileDrcInfo);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "fm_file_drc_info-通过id删除")
	@ApiOperation(value="fm_file_drc_info-通过id删除", notes="fm_file_drc_info-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		fmFileDrcInfoService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "fm_file_drc_info-批量删除")
	@ApiOperation(value="fm_file_drc_info-批量删除", notes="fm_file_drc_info-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.fmFileDrcInfoService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "fm_file_drc_info-通过id查询")
	@ApiOperation(value="fm_file_drc_info-通过id查询", notes="fm_file_drc_info-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		FmFileDrcInfo fmFileDrcInfo = fmFileDrcInfoService.getById(id);
		if(fmFileDrcInfo==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(fmFileDrcInfo);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param fmFileDrcInfo
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, FmFileDrcInfo fmFileDrcInfo) {
        return super.exportXls(request, fmFileDrcInfo, FmFileDrcInfo.class, "fm_file_drc_info");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, FmFileDrcInfo.class);
    }

	 /**
	  *   添加收藏及明细记录
	  *
	  * @param
	  * @return
	  */
	 @AutoLog(value = "文件下载阅读收藏信息表-添加")
	 @ApiOperation(value="文件下载阅读收藏信息表-添加", notes="文件下载阅读收藏信息表-添加")
	 @PostMapping(value = "/addall")
	 public Result<?> add(@RequestBody Map<String,Object> map) {

		 boolean res = fmFileDrcInfoService.addall(map);
		 return res == true ?  Result.ok("添加成功！"): Result.error("添加失败！");

	 }

	 /**
	  * 通过fileid查询下载阅读和收藏情况
	  *
	  * @param fileid
	  * @return
	  */
	 @AutoLog(value = "点击文件同时告知drc情况")
	 @ApiOperation(value="收藏文件详情表-通过pid查询", notes="收藏文件详情表-通过pid查询")
	 @GetMapping(value = "/getByFiled")
	 public Result<?> getByFiled(@RequestParam(name="fileid",required=true) String fileid,
								 @RequestParam(name="userid",required=false) String userid

	 ) {
		 List<DRCByFileid> fileDetailsInfo = fmFileDrcInfoService.getByFiled(fileid,userid);

		 return Result.ok(fileDetailsInfo);
	 }

}
