package org.jeecg.modules.filemanage.base.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.filemanage.base.entity.FmLikesDetails;
import org.jeecg.modules.filemanage.base.service.IFmLikesDetailsService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 点赞表
 * @Author: jeecg-boot
 * @Date:   2021-04-20
 * @Version: V1.0
 */
@Api(tags="点赞表")
@RestController
@RequestMapping("/base/fmLikesDetails")
@Slf4j
public class FmLikesDetailsController extends JeecgController<FmLikesDetails, IFmLikesDetailsService> {
	@Autowired
	private IFmLikesDetailsService fmLikesDetailsService;

	/**
	 * 分页列表查询
	 *
	 * @param fmLikesDetails
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "点赞表-分页列表查询")
	@ApiOperation(value="点赞表-分页列表查询", notes="点赞表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(FmLikesDetails fmLikesDetails,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<FmLikesDetails> queryWrapper = QueryGenerator.initQueryWrapper(fmLikesDetails, req.getParameterMap());
		Page<FmLikesDetails> page = new Page<FmLikesDetails>(pageNo, pageSize);
		IPage<FmLikesDetails> pageList = fmLikesDetailsService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
	 *   添加
	 *
	 * @param fmLikesDetails
	 * @return
	 */
	@AutoLog(value = "点赞表-添加")
	@ApiOperation(value="点赞表-添加", notes="点赞表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody FmLikesDetails fmLikesDetails) {
		fmLikesDetailsService.save(fmLikesDetails);
		return Result.ok("操作成功！");
	}

	/**
	 *  编辑
	 *
	 * @param fmLikesDetails
	 * @return
	 */
	@AutoLog(value = "点赞表-编辑")
	@ApiOperation(value="点赞表-编辑", notes="点赞表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody FmLikesDetails fmLikesDetails) {
		fmLikesDetailsService.updateById(fmLikesDetails);
		return Result.ok("操作成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "点赞表-通过id删除")
	@ApiOperation(value="点赞表-通过id删除", notes="点赞表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		fmLikesDetailsService.removeById(id);
		return Result.ok("操作成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "点赞表-批量删除")
	@ApiOperation(value="点赞表-批量删除", notes="点赞表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.fmLikesDetailsService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "点赞表-通过id查询")
	@ApiOperation(value="点赞表-通过id查询", notes="点赞表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		FmLikesDetails fmLikesDetails = fmLikesDetailsService.getById(id);
		if(fmLikesDetails==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(fmLikesDetails);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param fmLikesDetails
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, FmLikesDetails fmLikesDetails) {
        return super.exportXls(request, fmLikesDetails, FmLikesDetails.class, "点赞表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, FmLikesDetails.class);
    }

	 /**
	  *   添加
	  *
	  * @param fmLikesDetails
	  * @return
	  */
	 @AutoLog(value = "点赞表-添加")
	 @ApiOperation(value="点赞表-添加", notes="点赞表-添加")
	 @PostMapping(value = "/addlike")
	 public Result<?> addlike(@RequestBody FmLikesDetails fmLikesDetails) {
		boolean  res = fmLikesDetailsService.addlike(fmLikesDetails);
		 return res==true ?Result.ok("操作成功！"): Result.error("操作失败！");
	 }

}
