package org.jeecg.modules.filemanage.base.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.filemanage.base.entity.FmTypeDetails;
import org.jeecg.modules.filemanage.base.service.IFmTypeDetailsService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.filemanage.base.vo.fileTypesView;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: fm_type_details
 * @Author: jeecg-boot
 * @Date:   2021-04-14
 * @Version: V1.0
 */
@Api(tags="fm_type_details")
@RestController
@RequestMapping("/base/fmTypeDetails")
@Slf4j
public class FmTypeDetailsController extends JeecgController<FmTypeDetails, IFmTypeDetailsService>{
	@Autowired
	private IFmTypeDetailsService fmTypeDetailsService;

	/**
	 * 分页列表查询
	 *
	 * @param fmTypeDetails
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "fm_type_details-分页列表查询")
	@ApiOperation(value="fm_type_details-分页列表查询", notes="fm_type_details-分页列表查询")
	@GetMapping(value = "/rootList")
	public Result<?> queryPageList(FmTypeDetails fmTypeDetails,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		String parentId = fmTypeDetails.getPid();
		if (oConvertUtils.isEmpty(parentId)) {
			parentId = "0";
		}
		fmTypeDetails.setPid(null);
		QueryWrapper<FmTypeDetails> queryWrapper = QueryGenerator.initQueryWrapper(fmTypeDetails, req.getParameterMap());
		// 使用 eq 防止模糊查询
		queryWrapper.eq("pid", parentId);
		Page<FmTypeDetails> page = new Page<FmTypeDetails>(pageNo, pageSize);
		IPage<FmTypeDetails> pageList = fmTypeDetailsService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	 /**
      * 获取子数据
      * @param FmTypeDetails
      * @param req
      * @return
      */
	@AutoLog(value = "fm_type_details-获取子数据")
	@ApiOperation(value="fm_type_details-获取子数据", notes="fm_type_details-获取子数据")
	@GetMapping(value = "/childList")
	public Result<?> queryPageList(FmTypeDetails fmTypeDetails,HttpServletRequest req) {
		QueryWrapper<FmTypeDetails> queryWrapper = QueryGenerator.initQueryWrapper(fmTypeDetails, req.getParameterMap());
		List<FmTypeDetails> list = fmTypeDetailsService.list(queryWrapper);
		IPage<FmTypeDetails> pageList = new Page<>(1, 10, list.size());
        pageList.setRecords(list);
		return Result.ok(pageList);
	}

    /**
      * 批量查询子节点
      * @param parentIds 父ID（多个采用半角逗号分割）
      * @return 返回 IPage
      * @param parentIds
      * @return
      */
	@AutoLog(value = "fm_type_details-批量获取子数据")
    @ApiOperation(value="fm_type_details-批量获取子数据", notes="fm_type_details-批量获取子数据")
    @GetMapping("/getChildListBatch")
    public Result getChildListBatch(@RequestParam("parentIds") String parentIds) {
        try {
            QueryWrapper<FmTypeDetails> queryWrapper = new QueryWrapper<>();
            List<String> parentIdList = Arrays.asList(parentIds.split(","));
            queryWrapper.in("pid", parentIdList);
            List<FmTypeDetails> list = fmTypeDetailsService.list(queryWrapper);
            IPage<FmTypeDetails> pageList = new Page<>(1, 10, list.size());
            pageList.setRecords(list);
            return Result.ok(pageList);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("批量查询子节点失败：" + e.getMessage());
        }
    }

	/**
	 *   添加
	 *
	 * @param fmTypeDetails
	 * @return
	 */
	@AutoLog(value = "fm_type_details-添加")
	@ApiOperation(value="fm_type_details-添加", notes="fm_type_details-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody FmTypeDetails fmTypeDetails) {
		fmTypeDetailsService.addFmTypeDetails(fmTypeDetails);
		return Result.ok("添加成功！");
	}

	/**
	 *  编辑
	 *
	 * @param fmTypeDetails
	 * @return
	 */
	@AutoLog(value = "fm_type_details-编辑")
	@ApiOperation(value="fm_type_details-编辑", notes="fm_type_details-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody FmTypeDetails fmTypeDetails) {
		fmTypeDetailsService.updateFmTypeDetails(fmTypeDetails);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "fm_type_details-通过id删除")
	@ApiOperation(value="fm_type_details-通过id删除", notes="fm_type_details-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		fmTypeDetailsService.deleteFmTypeDetails(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "fm_type_details-批量删除")
	@ApiOperation(value="fm_type_details-批量删除", notes="fm_type_details-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.fmTypeDetailsService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功！");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "fm_type_details-通过id查询")
	@ApiOperation(value="fm_type_details-通过id查询", notes="fm_type_details-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		FmTypeDetails fmTypeDetails = fmTypeDetailsService.getById(id);
		if(fmTypeDetails==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(fmTypeDetails);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param fmTypeDetails
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, FmTypeDetails fmTypeDetails) {
		return super.exportXls(request, fmTypeDetails, FmTypeDetails.class, "fm_type_details");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
		return super.importExcel(request, response, FmTypeDetails.class);
    }

	 /**
	  * 动态获取级别
	  *
	  * @return
	  */
	 @AutoLog(value = "fm_type_details-动态获取类别")
	 @ApiOperation(value="fm_type_details-动态获取类别", notes="fm_type_details-动态获取类别")
	 @GetMapping(value = "/queryTypes")
	 public Result<?> queryTypes(@RequestParam(name="depth",required=true) Integer depth,
								 @RequestParam(name="num",required=true) Integer num,
								 @RequestParam(name="modid",required=true) String modid

	 ) {
		 List<fileTypesView> res = fmTypeDetailsService.queryTypes(depth,num,modid);
		 if(res==null) {
			 return Result.error("未找到对应数据");
		 }
		 return Result.ok(res);
	 }

}
