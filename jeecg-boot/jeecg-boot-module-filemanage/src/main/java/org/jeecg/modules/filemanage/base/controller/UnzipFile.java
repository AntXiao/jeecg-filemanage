package org.jeecg.modules.filemanage.base.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.modules.filemanage.base.service.IUnzipFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;


/**
 * @Description: 解压缩导入文件
 * @Author: jeecg-boot
 * @Date:   2020-12-08
 * @Version: V1.0
 */
@Api(tags="解压缩导入文件")
@RestController
@RequestMapping("base/Unzip")
@Slf4j
public class UnzipFile {
    @Autowired
    private IUnzipFileService unzipFileService;


    @AutoLog(value = "解压缩导入文件", operateType = CommonConstant.OPERATE_TYPE_7)
    @ApiOperation(value = "解压缩导入文件", notes = "解压缩导入文件")
    @PostMapping(value = "/importzip")
    public Result<?> unZip(MultipartFile srcFile) throws RuntimeException {
//
        boolean res =  unzipFileService.unZip(srcFile);
        return res==true? Result.ok("导入数据包成功"): Result.error("导入数据包失败");
    }



    @AutoLog(value = "导出文件", operateType = CommonConstant.OPERATE_TYPE_8)
    @ApiOperation(value = "导出文件", notes = "导出文件")
    @PutMapping(value = "/exportzip")
    public Result<?> exportzip(@RequestParam ArrayList<String> fileIds , String branchId){

        String res = unzipFileService.exportzip(fileIds,branchId);
        return Result.ok(res);
    }



}
