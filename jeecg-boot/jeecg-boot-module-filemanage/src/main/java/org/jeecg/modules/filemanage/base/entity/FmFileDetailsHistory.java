package org.jeecg.modules.filemanage.base.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: fm_file_details_history
 * @Author: jeecg-boot
 * @Date:   2021-04-13
 * @Version: V1.0
 */
@Data
@TableName("fm_file_details_history")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="fm_file_details_history对象", description="fm_file_details_history")
public class FmFileDetailsHistory implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**密级*/
	@Excel(name = "密级", width = 15)
    @ApiModelProperty(value = "密级")
    private java.lang.Integer secretLev;
	/**编号*/
	@Excel(name = "编号", width = 15)
    @ApiModelProperty(value = "编号")
    private java.lang.String fileCode;
	/**文件名*/
	@Excel(name = "文件名", width = 15)
    @ApiModelProperty(value = "文件名")
    private java.lang.String fileTitle;
	/**编制单位*/
	@Excel(name = "编制单位", width = 15)
    @ApiModelProperty(value = "编制单位")
    private java.lang.String orgBelong;
	/**文件描述*/
	@Excel(name = "文件描述", width = 15)
    @ApiModelProperty(value = "文件描述")
    private java.lang.String fileDescription;
	/**所属舰船冗余*/
	@Excel(name = "所属舰船冗余", width = 15)
    @ApiModelProperty(value = "所属舰船冗余")
    private java.lang.String boatBelong;
	/**所属系统*/
	@Excel(name = "所属系统", width = 15)
    @ApiModelProperty(value = "所属系统")
    private java.lang.String sysBelong;
	/**所属装备*/
	@Excel(name = "所属装备", width = 15)
    @ApiModelProperty(value = "所属装备")
    private java.lang.String eqpBelong;
	/**文件id*/
	@Excel(name = "文件id", width = 15)
    @ApiModelProperty(value = "文件id")
    private java.lang.String fileid;
	/**0无操作，1新增版本，2修改，3删除*/
	@Excel(name = "0无操作，1新增版本，2修改，3删除", width = 15)
    @ApiModelProperty(value = "0无操作，1新增版本，2修改，3删除")
    private java.lang.Integer opt;
	/**当时操作的版本号*/
	@Excel(name = "当时操作的版本号", width = 15)
    @ApiModelProperty(value = "当时操作的版本号")
    private java.lang.Double versionNo;
	/**文件路径*/
	@Excel(name = "文件路径", width = 15)
    @ApiModelProperty(value = "文件路径")
    private java.lang.String filePath;
	/**文件大小(B)*/
	@Excel(name = "文件大小(B)", width = 15)
    @ApiModelProperty(value = "文件大小(B)")
    private java.lang.Integer fileSize;
	/**文件类型*/
	@Excel(name = "文件类型", width = 15)
    @ApiModelProperty(value = "文件类型")
    private java.lang.String fileTypes;
	/**混淆名*/
	@Excel(name = "混淆名", width = 15)
    @ApiModelProperty(value = "混淆名")
    private java.lang.String fileMixName;
	/**上级节点id*/
	@Excel(name = "上级节点id", width = 15)
    @ApiModelProperty(value = "上级节点id")
    private java.lang.String pid;
	/**模块名*/
	@Excel(name = "模块名", width = 15)
    @ApiModelProperty(value = "模块名")
    private java.lang.String modelName;
	/**舷号*/
	@Excel(name = "舷号", width = 15)
    @ApiModelProperty(value = "舷号")
    private java.lang.String sideNum;
	/**所属设备uuid*/
	@Excel(name = "所属设备uuid", width = 15)
    @ApiModelProperty(value = "所属设备uuid")
    private java.lang.String eqpUid;
	/**所属系统编号*/
	@Excel(name = "所属系统编号", width = 15)
    @ApiModelProperty(value = "所属系统编号")
    private java.lang.String systemNo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getSysOrgCode() {
        return sysOrgCode;
    }

    public void setSysOrgCode(String sysOrgCode) {
        this.sysOrgCode = sysOrgCode;
    }

    public Integer getSecretLev() {
        return secretLev;
    }

    public void setSecretLev(Integer secretLev) {
        this.secretLev = secretLev;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public String getFileTitle() {
        return fileTitle;
    }

    public void setFileTitle(String fileTitle) {
        this.fileTitle = fileTitle;
    }

    public String getOrgBelong() {
        return orgBelong;
    }

    public void setOrgBelong(String orgBelong) {
        this.orgBelong = orgBelong;
    }

    public String getFileDescription() {
        return fileDescription;
    }

    public void setFileDescription(String fileDescription) {
        this.fileDescription = fileDescription;
    }

    public String getBoatBelong() {
        return boatBelong;
    }

    public void setBoatBelong(String boatBelong) {
        this.boatBelong = boatBelong;
    }

    public String getSysBelong() {
        return sysBelong;
    }

    public void setSysBelong(String sysBelong) {
        this.sysBelong = sysBelong;
    }

    public String getEqpBelong() {
        return eqpBelong;
    }

    public void setEqpBelong(String eqpBelong) {
        this.eqpBelong = eqpBelong;
    }

    public String getFileid() {
        return fileid;
    }

    public void setFileid(String fileid) {
        this.fileid = fileid;
    }

    public Integer getOpt() {
        return opt;
    }

    public void setOpt(Integer opt) {
        this.opt = opt;
    }

    public Double getVersionNo() {
        return versionNo;
    }

    public void setVersionNo(Double versionNo) {
        this.versionNo = versionNo;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Integer getFileSize() {
        return fileSize;
    }

    public void setFileSize(Integer fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileTypes() {
        return fileTypes;
    }

    public void setFileTypes(String fileTypes) {
        this.fileTypes = fileTypes;
    }

    public String getFileMixName() {
        return fileMixName;
    }

    public void setFileMixName(String fileMixName) {
        this.fileMixName = fileMixName;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getSideNum() {
        return sideNum;
    }

    public void setSideNum(String sideNum) {
        this.sideNum = sideNum;
    }

    public String getEqpUid() {
        return eqpUid;
    }

    public void setEqpUid(String eqpUid) {
        this.eqpUid = eqpUid;
    }

    public String getSystemNo() {
        return systemNo;
    }

    public void setSystemNo(String systemNo) {
        this.systemNo = systemNo;
    }
}
