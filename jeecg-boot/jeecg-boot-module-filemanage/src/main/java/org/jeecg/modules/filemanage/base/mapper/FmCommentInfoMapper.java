package org.jeecg.modules.filemanage.base.mapper;

import java.util.List;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import org.jeecg.modules.filemanage.base.entity.FmCommentInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 评论表
 * @Author: jeecg-boot
 * @Date:   2021-04-20
 * @Version: V1.0
 */
public interface FmCommentInfoMapper extends BaseMapper<FmCommentInfo> {
    List<FmCommentInfo>queryPagelikeList(Page<FmCommentInfo> page,String userid, String fileid);

    Integer updateLikeById(String id,Integer status);

}
