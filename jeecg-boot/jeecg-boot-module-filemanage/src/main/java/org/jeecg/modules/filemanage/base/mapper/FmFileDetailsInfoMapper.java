package org.jeecg.modules.filemanage.base.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.filemanage.base.entity.FmFileDetailsInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.filemanage.base.vo.DMFileList;

/**
 * @Description: fm_file_details_info
 * @Author: jeecg-boot
 * @Date:   2021-04-13
 * @Version: V1.0
 */
public interface FmFileDetailsInfoMapper extends BaseMapper<FmFileDetailsInfo> {
    List<FmFileDetailsInfo> queryPagelistNopid(Page<FmFileDetailsInfo> page, String userId, String msgCategory);

    List<DMFileList>getByPid(String pid, String fileTitle, String fileCode, Integer secretLev, String eqpBelong);

    Integer setPid(String whereSql);
}
