package org.jeecg.modules.filemanage.base.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.filemanage.base.entity.FmModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 模式菜单表
 * @Author: jeecg-boot
 * @Date:   2021-06-09
 * @Version: V1.0
 */
public interface FmModelMapper extends BaseMapper<FmModel> {

}
