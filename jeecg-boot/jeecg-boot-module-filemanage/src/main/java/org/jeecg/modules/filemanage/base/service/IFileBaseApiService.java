package org.jeecg.modules.filemanage.base.service;

import org.springframework.web.multipart.MultipartFile;

public interface IFileBaseApiService {
    void readFileStream(MultipartFile mf, String fileId);
}
