package org.jeecg.modules.filemanage.base.service;

import java.util.Map;

/**
 * @Description: 文件上传下载事务处理接口
 * @Author: chris
 * @Date:   2020-08-21
 * @Version: V1.0
 */
public interface IFileTransManageService {
   boolean add(Map<String, Object> map);

   boolean edit(Map<String, Object> map);

   boolean delete(String ids);
}
