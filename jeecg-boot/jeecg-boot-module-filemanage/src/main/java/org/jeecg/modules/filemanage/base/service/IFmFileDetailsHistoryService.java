package org.jeecg.modules.filemanage.base.service;

import org.jeecg.modules.filemanage.base.entity.FmFileDetailsHistory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: fm_file_details_history
 * @Author: jeecg-boot
 * @Date:   2021-04-13
 * @Version: V1.0
 */
public interface IFmFileDetailsHistoryService extends IService<FmFileDetailsHistory> {

}
