package org.jeecg.modules.filemanage.base.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.filemanage.base.entity.FmFileDrcDetails;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.filemanage.base.vo.DMFileList;
import org.jeecg.modules.filemanage.base.vo.FileDrcMyList;

import java.util.List;

/**
 * @Description: fm_file_drc_details
 * @Author: jeecg-boot
 * @Date:   2021-04-13
 * @Version: V1.0
 */
public interface IFmFileDrcDetailsService extends IService<FmFileDrcDetails> {
    List<DMFileList> getByPid(String pid, String userid, String fileTitle, String fileCode, Integer secretLev, String fileTypes);

    Page<FileDrcMyList> mylist(Page<FileDrcMyList> pageList,String createBy,Integer drcType);
}
