package org.jeecg.modules.filemanage.base.service;

import org.jeecg.modules.filemanage.base.entity.FmFileDrcInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.filemanage.base.vo.DRCByFileid;

import java.util.List;
import java.util.Map;

/**
 * @Description: fm_file_drc_info
 * @Author: jeecg-boot
 * @Date:   2021-04-13
 * @Version: V1.0
 */
public interface IFmFileDrcInfoService extends IService<FmFileDrcInfo> {
    boolean addall(Map<String,Object> map);

    List<DRCByFileid> getByFiled(String fileid, String userid);
}
