package org.jeecg.modules.filemanage.base.service;

import org.jeecg.modules.filemanage.base.entity.FmFileExportRec;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: fm_file_export_rec
 * @Author: jeecg-boot
 * @Date:   2021-04-13
 * @Version: V1.0
 */
public interface IFmFileExportRecService extends IService<FmFileExportRec> {

}
