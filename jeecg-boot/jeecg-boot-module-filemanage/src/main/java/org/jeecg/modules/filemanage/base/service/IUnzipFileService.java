package org.jeecg.modules.filemanage.base.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;

public interface IUnzipFileService {
    boolean unZip(MultipartFile srcFile);

    String exportzip(ArrayList<String> fileIds, String branchId);
}
