package org.jeecg.modules.filemanage.base.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.modules.filemanage.base.entity.FmFileDetailsInfo;
import org.jeecg.modules.filemanage.base.entity.FmFileDrcDetails;
import org.jeecg.modules.filemanage.base.entity.FmTypeDetails;
import org.jeecg.modules.filemanage.base.mapper.*;
import org.jeecg.modules.filemanage.base.service.IBIReportService;
import org.jeecg.modules.filemanage.base.vo.commentView;
import org.jeecg.modules.filemanage.base.vo.fileInOutView;
import org.jeecg.modules.filemanage.base.vo.fileSearchView;
import org.jeecg.modules.filemanage.base.vo.fileTypeRadioView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class BIReportServiceImpl implements IBIReportService {
    @Autowired
    private BIReportMapper biReportMapper;

    @Autowired
    private ISysBaseAPI sysBaseAPI;

    @Autowired
    private FmFileDetailsInfoMapper fmFileDetailsInfoMapper;

    @Autowired
    private FmTypeDetailsMapper fmTypeDetailsMapper;

    @Autowired
    private FmFileDrcDetailsMapper fileDrcDetailsMapper;

    @Autowired
    private essearchImpl essearch;

    @Autowired
    private FmFractionInfoMapper fmFractionInfoMapper;

   public List<fileTypeRadioView> fileTypeRadio(String fileType){
        List<fileTypeRadioView> res = new ArrayList<>();
        res = this.biReportMapper.fileTypeRadio(fileType);
        return res;
    }

    public List<fileInOutView> fileInOut(String branchId){
        List<fileInOutView> res = new ArrayList<>();
        //取需求信息
        List<fileInOutView> requestList = biReportMapper.demandinfo(branchId);
       for(fileInOutView r :res){
           for(fileInOutView s:requestList)
           {
               if(s.getBranchid().equalsIgnoreCase(r.getBranchid()))
                   r.setRequestNum(s.getRequestNum());
           }
       }
        //取导出记录
        List<fileInOutView> supportList =  biReportMapper.exportinfo(branchId);
        for(fileInOutView r :res){
            for(fileInOutView s:supportList)
            {
                if(s.getBranchid().equalsIgnoreCase(r.getBranchid()))
                    r.setSupportNum(s.getSupportNum());
            }
        }

        return res;
    }

   public Page<fileSearchView>fileSearch (Page<fileSearchView> page,String keyword,String typeName,String suffix){
       //检查typeName是否为空
       if(typeName!=null && !typeName.isEmpty()){
           //不为空时，判断是大类名还是小类名
           QueryWrapper<FmTypeDetails> queryWrapper = new QueryWrapper<>();
           queryWrapper.eq("id",typeName);
           FmTypeDetails dt = fmTypeDetailsMapper.selectOne(queryWrapper);
           if(dt.getPid().equalsIgnoreCase("0"))//是一级类
           {
               typeName= "and pid='"+ dt.getId() +"'" ;
           }else if(!dt.getPid().equalsIgnoreCase("0")&&dt.getHasChild()!="1")
           {//二级类
               typeName= "and  id='"+ dt.getId() +"'" ;
           }else{
               //二级以下类别
               typeName= "and  id='"+ dt.getId() +"'" ;
           }
       }
       List<String> ja =  essearch.queryBykw(keyword);
       StringBuilder sb = new StringBuilder();
       String fileids = "";
       ja = ja.stream().distinct().collect(Collectors.toList()); //去重
       for(String a :ja){
          sb.append("'").append(a).append("',");
       }
       if(sb.length()>0)
            fileids = sb.toString().substring(0,sb.toString().length()-1);

       List<fileSearchView> fsvlist=biReportMapper.fileSearch(page,keyword,typeName,suffix,fileids);
       List<commentView> cvList = fmFractionInfoMapper.sumFraction(null);
       fsvlist.stream().map(s->cvList.stream().filter(f->s.getFileid()!=null&&f.getFileid()!=null&&s.getId().equalsIgnoreCase(f.getFileid()))
               .findFirst().map(f->{
                   s.setScore(f.getScore());
                   return s;
               }).orElse(null)).collect(Collectors.toList());
       fsvlist.sort(Comparator.comparing((fileSearchView a)->a.getCreateTime()).reversed());
       return page.setRecords(fsvlist);
    }

    @Override
    public Page<fileSearchView> fileHot(Page<fileSearchView> page, String userId, String msgCategory){
        return page.setRecords(biReportMapper.fileHot(page, userId, msgCategory));
    }

    public fileInOutView totalDoc(){
        fileInOutView f = new fileInOutView();
        QueryWrapper<FmFileDetailsInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("id");
        f.setRequestNum(sysBaseAPI.queryAllUser().size());
        f.setSupportNum(fmFileDetailsInfoMapper.selectCount(queryWrapper));
        return  f;
    }

    public Page<fileSearchView>fileNew(Page<fileSearchView> page, String typeId, String msgCategory){
        return page.setRecords(biReportMapper.fileNew(page, typeId, msgCategory));
    }

    public fileInOutView userFileInfo(String userid){
        fileInOutView f = new fileInOutView();
        QueryWrapper<FmFileDetailsInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("create_by",userid);
        f.setRequestNum(fmFileDetailsInfoMapper.selectCount(queryWrapper));
        QueryWrapper<FmFileDrcDetails> queryWrapper2 = new QueryWrapper<>();
        queryWrapper2.eq("create_by",userid).eq("drc_type",2);
        f.setSupportNum(fileDrcDetailsMapper.selectCount(queryWrapper2));
        return f;
    }

    public List<String> essearch(String kw){
       List<String> ja =  essearch.queryBykw(kw);
       return ja;
    }
}
