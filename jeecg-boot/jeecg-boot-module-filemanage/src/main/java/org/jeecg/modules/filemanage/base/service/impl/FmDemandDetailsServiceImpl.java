package org.jeecg.modules.filemanage.base.service.impl;

import org.jeecg.modules.filemanage.base.entity.FmDemandDetails;
import org.jeecg.modules.filemanage.base.mapper.FmDemandDetailsMapper;
import org.jeecg.modules.filemanage.base.service.IFmDemandDetailsService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: fm_demand_details
 * @Author: jeecg-boot
 * @Date:   2021-04-13
 * @Version: V1.0
 */
@Service
public class FmDemandDetailsServiceImpl extends ServiceImpl<FmDemandDetailsMapper, FmDemandDetails> implements IFmDemandDetailsService {

}
