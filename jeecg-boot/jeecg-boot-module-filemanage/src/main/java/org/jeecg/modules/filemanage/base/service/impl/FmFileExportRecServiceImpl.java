package org.jeecg.modules.filemanage.base.service.impl;

import org.jeecg.modules.filemanage.base.entity.FmFileExportRec;
import org.jeecg.modules.filemanage.base.mapper.FmFileExportRecMapper;
import org.jeecg.modules.filemanage.base.service.IFmFileExportRecService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: fm_file_export_rec
 * @Author: jeecg-boot
 * @Date:   2021-04-13
 * @Version: V1.0
 */
@Service
public class FmFileExportRecServiceImpl extends ServiceImpl<FmFileExportRecMapper, FmFileExportRec> implements IFmFileExportRecService {

}
