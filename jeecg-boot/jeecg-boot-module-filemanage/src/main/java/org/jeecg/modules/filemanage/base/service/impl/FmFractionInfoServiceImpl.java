package org.jeecg.modules.filemanage.base.service.impl;

import org.jeecg.modules.filemanage.base.entity.FmFractionInfo;
import org.jeecg.modules.filemanage.base.mapper.FmFractionInfoMapper;
import org.jeecg.modules.filemanage.base.service.IFmFractionInfoService;
import org.jeecg.modules.filemanage.base.vo.commentView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: fm_fraction_info
 * @Author: jeecg-boot
 * @Date:   2021-06-21
 * @Version: V1.0
 */
@Service
public class FmFractionInfoServiceImpl extends ServiceImpl<FmFractionInfoMapper, FmFractionInfo> implements IFmFractionInfoService {

    @Autowired
    private FmFractionInfoMapper fmFractionInfoMapper;

   public List<commentView> sumfraction(String fileid){
       List<commentView> fraction = new ArrayList<>();
        //取当前文章，每个人所评最高分的平均分
        fraction = fmFractionInfoMapper.sumFraction(fileid);
        return fraction;
    }
}
