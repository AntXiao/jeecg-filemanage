package org.jeecg.modules.filemanage.base.service.impl;

import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.filemanage.base.entity.FmTypeDetails;
import org.jeecg.modules.filemanage.base.mapper.FmTypeDetailsMapper;
import org.jeecg.modules.filemanage.base.service.IFmTypeDetailsService;
import org.jeecg.modules.filemanage.base.vo.fileTypesView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: fm_type_details
 * @Author: jeecg-boot
 * @Date:   2021-04-14
 * @Version: V1.0
 */
@Service
public class FmTypeDetailsServiceImpl extends ServiceImpl<FmTypeDetailsMapper, FmTypeDetails> implements IFmTypeDetailsService {
	@Autowired
	private FmTypeDetailsMapper fmTypeDetailsMapper;

	@Override
	public void addFmTypeDetails(FmTypeDetails fmTypeDetails) {
		if(oConvertUtils.isEmpty(fmTypeDetails.getPid())){
			fmTypeDetails.setPid(IFmTypeDetailsService.ROOT_PID_VALUE);
		}else{
			//如果当前节点父ID不为空 则设置父节点的hasChildren 为1
			FmTypeDetails parent = baseMapper.selectById(fmTypeDetails.getPid());
			if(parent!=null && !"1".equals(parent.getHasChild())){
				parent.setHasChild("1");
				baseMapper.updateById(parent);
			}
		}
		baseMapper.insert(fmTypeDetails);
	}

	@Override
	public void updateFmTypeDetails(FmTypeDetails fmTypeDetails) {
		FmTypeDetails entity = this.getById(fmTypeDetails.getId());
		if(entity==null) {
			throw new JeecgBootException("未找到对应实体");
		}
		String old_pid = entity.getPid();
		String new_pid = fmTypeDetails.getPid();
		if(!old_pid.equals(new_pid)) {
			updateOldParentNode(old_pid);
			if(oConvertUtils.isEmpty(new_pid)){
				fmTypeDetails.setPid(IFmTypeDetailsService.ROOT_PID_VALUE);
			}
			if(!IFmTypeDetailsService.ROOT_PID_VALUE.equals(fmTypeDetails.getPid())) {
				baseMapper.updateTreeNodeStatus(fmTypeDetails.getPid(), IFmTypeDetailsService.HASCHILD);
			}
		}
		baseMapper.updateById(fmTypeDetails);
	}

	@Override
	public void deleteFmTypeDetails(String id) throws JeecgBootException {
		FmTypeDetails fmTypeDetails = this.getById(id);
		if(fmTypeDetails==null) {
			throw new JeecgBootException("未找到对应实体");
		}
		updateOldParentNode(fmTypeDetails.getPid());
		baseMapper.deleteById(id);
	}



	/**
	 * 根据所传pid查询旧的父级节点的子节点并修改相应状态值
	 * @param pid
	 */
	private void updateOldParentNode(String pid) {
		if(!IFmTypeDetailsService.ROOT_PID_VALUE.equals(pid)) {
			Integer count = baseMapper.selectCount(new QueryWrapper<FmTypeDetails>().eq("pid", pid));
			if(count==null || count<=1) {
				baseMapper.updateTreeNodeStatus(pid, IFmTypeDetailsService.NOCHILD);
			}
		}
	}

	public List<fileTypesView> queryTypes(int depth, int num ,String modid){
		List<fileTypesView> res = new ArrayList<>();
		List<fileTypesView> items = new ArrayList<>();
		items = fmTypeDetailsMapper.queryTypes(depth,num,modid);
		if(depth>1) {
			res = fmTypeDetailsMapper.queryTypes(depth - 1, num,modid);
			for (fileTypesView f : res) {
				for (fileTypesView k : items) {
					if (k.getPid().equalsIgnoreCase(f.getId())) {
						if (f.getItems() == null)
							f.setItems(new ArrayList<fileTypesView>());
						f.getItems().add(k);
					}
				}
			}
		}else{
			res = items;
		}
		return res;
	}

}
