package org.jeecg.modules.filemanage.base.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: 文件下载阅读收藏信息表
 * @Author: chris
 * @Date:   2020-09-02
 * @Version: V1.0
 */
@Data
public class DRCByFileid {
    /**主键*/
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;
    /**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
    /**下载次数*/
    @Excel(name = "下载次数", width = 15)
    @ApiModelProperty(value = "下载次数")
    private Integer downloadTimes;
    /**阅读次数*/
    @Excel(name = "阅读次数", width = 15)
    @ApiModelProperty(value = "阅读次数")
    private Integer readTimes;
    /**收藏次数*/
    @Excel(name = "收藏次数", width = 15)
    @ApiModelProperty(value = "收藏次数")
    private Integer collectTimes;
    /**关联文件表id*/
    @Excel(name = "关联文件表id", width = 15)
    @ApiModelProperty(value = "关联文件表id")
    private String fileId;

    /**是否收藏过*/
    @Excel(name = "是否收藏过", width = 15)
    @ApiModelProperty(value = "关联文件表id")
    private Boolean isCollected;
}


