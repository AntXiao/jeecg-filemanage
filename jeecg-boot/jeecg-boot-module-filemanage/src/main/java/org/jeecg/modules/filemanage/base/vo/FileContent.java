package org.jeecg.modules.filemanage.base.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class FileContent implements Serializable {
    private String fileid;

    private String content;
}
