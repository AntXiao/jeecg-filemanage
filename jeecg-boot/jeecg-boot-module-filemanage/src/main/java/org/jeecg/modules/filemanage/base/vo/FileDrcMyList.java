package org.jeecg.modules.filemanage.base.vo;

import lombok.Data;
import org.jeecg.modules.filemanage.base.entity.FmFileDrcDetails;


@Data
public class FileDrcMyList extends FmFileDrcDetails {
    private String fileTitle;

    private String fid;
}
