package org.jeecg.modules.filemanage.base.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value="BI报表饼图对象", description="fileTypeRadioView")
public class fileTypeRadioView {
    private String fileType;

    private Integer fileCount;
}
