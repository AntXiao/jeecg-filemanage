package org.jeecg.modules.filemanage.base.vo;


import lombok.Data;

import java.util.List;


//文件分类自定义界别查询
@Data
public class fileTypesView {
    private String id;

    private String typeName;

    private String pid;

    List<fileTypesView> items;
}
