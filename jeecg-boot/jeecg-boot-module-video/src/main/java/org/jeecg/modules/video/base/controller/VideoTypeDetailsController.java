package org.jeecg.modules.video.base.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.video.base.entity.VideoTypeDetails;
import org.jeecg.modules.video.base.service.IVideoTypeDetailsService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: video_type_details
 * @Author: jeecg-boot
 * @Date:   2021-06-28
 * @Version: V1.0
 */
@Api(tags="video_type_details")
@RestController
@RequestMapping("/base/videoTypeDetails")
@Slf4j
public class VideoTypeDetailsController extends JeecgController<VideoTypeDetails, IVideoTypeDetailsService>{
	@Autowired
	private IVideoTypeDetailsService videoTypeDetailsService;

	/**
	 * 分页列表查询
	 *
	 * @param videoTypeDetails
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "video_type_details-分页列表查询")
	@ApiOperation(value="video_type_details-分页列表查询", notes="video_type_details-分页列表查询")
	@GetMapping(value = "/rootList")
	public Result<?> queryPageList(VideoTypeDetails videoTypeDetails,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		String parentId = videoTypeDetails.getPid();
		if (oConvertUtils.isEmpty(parentId)) {
			parentId = "0";
		}
		videoTypeDetails.setPid(null);
		QueryWrapper<VideoTypeDetails> queryWrapper = QueryGenerator.initQueryWrapper(videoTypeDetails, req.getParameterMap());
		// 使用 eq 防止模糊查询
		queryWrapper.eq("pid", parentId);
		queryWrapper.orderByAsc("lv_idx");
		Page<VideoTypeDetails> page = new Page<VideoTypeDetails>(pageNo, pageSize);
		IPage<VideoTypeDetails> pageList = videoTypeDetailsService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	 /**
      * 获取子数据
      * @param
      * @param req
      * @return
      */
	@AutoLog(value = "video_type_details-获取子数据")
	@ApiOperation(value="video_type_details-获取子数据", notes="video_type_details-获取子数据")
	@GetMapping(value = "/childList")
	public Result<?> queryPageList(VideoTypeDetails videoTypeDetails,HttpServletRequest req) {
		QueryWrapper<VideoTypeDetails> queryWrapper = QueryGenerator.initQueryWrapper(videoTypeDetails, req.getParameterMap());
		List<VideoTypeDetails> list = videoTypeDetailsService.list(queryWrapper);
		IPage<VideoTypeDetails> pageList = new Page<>(1, 10, list.size());
        pageList.setRecords(list);
		return Result.ok(pageList);
	}

    /**
      * 批量查询子节点
      * @param parentIds 父ID（多个采用半角逗号分割）
      * @return 返回 IPage
      * @param parentIds
      * @return
      */
	@AutoLog(value = "video_type_details-批量获取子数据")
    @ApiOperation(value="video_type_details-批量获取子数据", notes="video_type_details-批量获取子数据")
    @GetMapping("/getChildListBatch")
    public Result getChildListBatch(@RequestParam("parentIds") String parentIds) {
        try {
            QueryWrapper<VideoTypeDetails> queryWrapper = new QueryWrapper<>();
            List<String> parentIdList = Arrays.asList(parentIds.split(","));
            queryWrapper.in("pid", parentIdList);
            List<VideoTypeDetails> list = videoTypeDetailsService.list(queryWrapper);
            IPage<VideoTypeDetails> pageList = new Page<>(1, 10, list.size());
            pageList.setRecords(list);
            return Result.ok(pageList);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("批量查询子节点失败：" + e.getMessage());
        }
    }

	/**
	 *   添加
	 *
	 * @param videoTypeDetails
	 * @return
	 */
	@AutoLog(value = "video_type_details-添加")
	@ApiOperation(value="video_type_details-添加", notes="video_type_details-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody VideoTypeDetails videoTypeDetails) {
		videoTypeDetailsService.addVideoTypeDetails(videoTypeDetails);
		return Result.ok("添加成功！");
	}

	/**
	 *  编辑
	 *
	 * @param videoTypeDetails
	 * @return
	 */
	@AutoLog(value = "video_type_details-编辑")
	@ApiOperation(value="video_type_details-编辑", notes="video_type_details-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody VideoTypeDetails videoTypeDetails) {
		videoTypeDetailsService.updateVideoTypeDetails(videoTypeDetails);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "video_type_details-通过id删除")
	@ApiOperation(value="video_type_details-通过id删除", notes="video_type_details-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		videoTypeDetailsService.deleteVideoTypeDetails(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "video_type_details-批量删除")
	@ApiOperation(value="video_type_details-批量删除", notes="video_type_details-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.videoTypeDetailsService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功！");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "video_type_details-通过id查询")
	@ApiOperation(value="video_type_details-通过id查询", notes="video_type_details-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		VideoTypeDetails videoTypeDetails = videoTypeDetailsService.getById(id);
		if(videoTypeDetails==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(videoTypeDetails);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param videoTypeDetails
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, VideoTypeDetails videoTypeDetails) {
		return super.exportXls(request, videoTypeDetails, VideoTypeDetails.class, "video_type_details");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
		return super.importExcel(request, response, VideoTypeDetails.class);
    }

	 /**
	  * 查询目录列表
	  *
	  * @param
	  * @return
	  */
	 @GetMapping(value = "/catalogueList")
	 public Result<?> catalogueList() {
		// this.videoTypeDetailsService.catalogueList();
		 return Result.ok();
	 }

}
