package org.jeecg.modules.video.base.service;

import org.jeecg.modules.video.base.entity.VideoFileDetails;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: video_file_details
 * @Author: jeecg-boot
 * @Date:   2021-06-25
 * @Version: V1.0
 */
public interface IVideoFileDetailsService extends IService<VideoFileDetails> {

}
