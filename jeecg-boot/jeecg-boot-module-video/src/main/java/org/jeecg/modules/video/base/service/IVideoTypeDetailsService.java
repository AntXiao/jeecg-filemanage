package org.jeecg.modules.video.base.service;

import org.jeecg.modules.video.base.entity.VideoTypeDetails;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.common.exception.JeecgBootException;

/**
 * @Description: video_type_details
 * @Author: jeecg-boot
 * @Date:   2021-06-28
 * @Version: V1.0
 */
public interface IVideoTypeDetailsService extends IService<VideoTypeDetails> {

	/**根节点父ID的值*/
	public static final String ROOT_PID_VALUE = "0";
	
	/**树节点有子节点状态值*/
	public static final String HASCHILD = "1";
	
	/**树节点无子节点状态值*/
	public static final String NOCHILD = "0";

	/**新增节点*/
	void addVideoTypeDetails(VideoTypeDetails videoTypeDetails);
	
	/**修改节点*/
	void updateVideoTypeDetails(VideoTypeDetails videoTypeDetails) throws JeecgBootException;
	
	/**删除节点*/
	void deleteVideoTypeDetails(String id) throws JeecgBootException;

}
