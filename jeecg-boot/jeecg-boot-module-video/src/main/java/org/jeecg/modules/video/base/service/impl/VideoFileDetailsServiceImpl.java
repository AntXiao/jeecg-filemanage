package org.jeecg.modules.video.base.service.impl;

import org.jeecg.modules.video.base.entity.VideoFileDetails;
import org.jeecg.modules.video.base.mapper.VideoFileDetailsMapper;
import org.jeecg.modules.video.base.service.IVideoFileDetailsService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: video_file_details
 * @Author: jeecg-boot
 * @Date:   2021-06-25
 * @Version: V1.0
 */
@Service
public class VideoFileDetailsServiceImpl extends ServiceImpl<VideoFileDetailsMapper, VideoFileDetails> implements IVideoFileDetailsService {

}
