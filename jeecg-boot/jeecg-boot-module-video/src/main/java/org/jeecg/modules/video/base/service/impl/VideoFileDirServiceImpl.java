package org.jeecg.modules.video.base.service.impl;

import org.jeecg.modules.video.base.entity.VideoFileDir;
import org.jeecg.modules.video.base.mapper.VideoFileDirMapper;
import org.jeecg.modules.video.base.service.IVideoFileDirService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: video_file_dir
 * @Author: jeecg-boot
 * @Date:   2021-06-25
 * @Version: V1.0
 */
@Service
public class VideoFileDirServiceImpl extends ServiceImpl<VideoFileDirMapper, VideoFileDir> implements IVideoFileDirService {

}
