package org.jeecg.modules.video.base.service.impl;

import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.video.base.entity.VideoTypeDetails;
import org.jeecg.modules.video.base.mapper.VideoTypeDetailsMapper;
import org.jeecg.modules.video.base.service.IVideoTypeDetailsService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: video_type_details
 * @Author: jeecg-boot
 * @Date:   2021-06-28
 * @Version: V1.0
 */
@Service
public class VideoTypeDetailsServiceImpl extends ServiceImpl<VideoTypeDetailsMapper, VideoTypeDetails> implements IVideoTypeDetailsService {

	@Override
	public void addVideoTypeDetails(VideoTypeDetails videoTypeDetails) {
		if(oConvertUtils.isEmpty(videoTypeDetails.getPid())){
			videoTypeDetails.setPid(IVideoTypeDetailsService.ROOT_PID_VALUE);
		}else{
			//如果当前节点父ID不为空 则设置父节点的hasChildren 为1
			VideoTypeDetails parent = baseMapper.selectById(videoTypeDetails.getPid());
			if(parent!=null && !"1".equals(parent.getHasChild())){
				parent.setHasChild("1");
				baseMapper.updateById(parent);
			}
		}
		baseMapper.insert(videoTypeDetails);
	}
	
	@Override
	public void updateVideoTypeDetails(VideoTypeDetails videoTypeDetails) {
		VideoTypeDetails entity = this.getById(videoTypeDetails.getId());
		if(entity==null) {
			throw new JeecgBootException("未找到对应实体");
		}
		String old_pid = entity.getPid();
		String new_pid = videoTypeDetails.getPid();
		if(!old_pid.equals(new_pid)) {
			updateOldParentNode(old_pid);
			if(oConvertUtils.isEmpty(new_pid)){
				videoTypeDetails.setPid(IVideoTypeDetailsService.ROOT_PID_VALUE);
			}
			if(!IVideoTypeDetailsService.ROOT_PID_VALUE.equals(videoTypeDetails.getPid())) {
				baseMapper.updateTreeNodeStatus(videoTypeDetails.getPid(), IVideoTypeDetailsService.HASCHILD);
			}
		}
		baseMapper.updateById(videoTypeDetails);
	}
	
	@Override
	public void deleteVideoTypeDetails(String id) throws JeecgBootException {
		VideoTypeDetails videoTypeDetails = this.getById(id);
		if(videoTypeDetails==null) {
			throw new JeecgBootException("未找到对应实体");
		}
		updateOldParentNode(videoTypeDetails.getPid());
		baseMapper.deleteById(id);
	}
	
	
	
	/**
	 * 根据所传pid查询旧的父级节点的子节点并修改相应状态值
	 * @param pid
	 */
	private void updateOldParentNode(String pid) {
		if(!IVideoTypeDetailsService.ROOT_PID_VALUE.equals(pid)) {
			Integer count = baseMapper.selectCount(new QueryWrapper<VideoTypeDetails>().eq("pid", pid));
			if(count==null || count<=1) {
				baseMapper.updateTreeNodeStatus(pid, IVideoTypeDetailsService.NOCHILD);
			}
		}
	}

}
