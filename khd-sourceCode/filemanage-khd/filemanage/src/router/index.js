import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('@/views/index'),
    },
    {
      path: '/viewDoc/:id',
      name: 'viewDoc',
      component: () => import('@/components/ViewDoc'),
    },
    {
      path: '/searchDoc',
      name: 'searchDoc',
      component: () => import('@/views/searchDoc'),
    },
    {
      path: '/typeList',
      name: 'typeList',
      component: () => import('@/views/typeList'),
    },
    {
      path: '/customerCenter',
      name: 'customerCenter',
      component: () => import('@/views/customerCenter'),
    },
    {
      path: '/video',
      name: 'video',
      component: () => import('@/views/video'),
    },
  ]
})
